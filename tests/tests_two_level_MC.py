import numpy as np
import sys
sys.path.insert(0,'/data/user/albajacas_a/lassomontecarlo/src')
from two_level_MC import get_two_level_estimates
from LassoFixedN import LassoFixedN

N = 10**3
M = 10**5
xsN = np.random.randn(N)
xsM = np.random.randn(M)

def true_model(xs):
    return 3*xs + 4*xs**3 + 0.1 * np.random.randn(len(xs))

def surr_model(xs):
    return 3*xs


ys = true_model(xsN)
ysSurr = surr_model(xsN)
ysSurrM = surr_model(xsM)

print("Expected moments 0,1,0,3")
results = get_two_level_estimates(ys, ysSurr, ysSurrM, calculate_MSEs = True, adjust_alpha = True)
for key in results.keys():
    print(key, results[key])
    

